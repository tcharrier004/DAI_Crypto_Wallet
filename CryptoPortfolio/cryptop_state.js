//-------------------------------------------------------------------- State ---
// État de l'application avant affichage
//
state = {

  data: {
    // hasChanged: false,
    cryptos: {
      list       : {},
      listNum    : 0,
      filtered   : [],
      filteredNum: 0,
    },
    fiats: {
      list       : {},
      listNum    : 0,
      filtered   : [],
      filteredNum: 0,
    },
    coins: {
      allCodes : [],
      posValueCodes : [],
      nullValueCodes : [],
    },
  },
  ui: {
    currenciesCard: {
      tabs: {
        cryptos: {
          pagination: {
            nbPages: 1,
          },
        },
        fiats: {
          pagination: {
            nbPages: 1,
          },
        },
      },
    },
  },
  representations: {
    app: '',
    currencies: '',
    preferences: '',
    wallet: '',
  },

  samUpdate(model) {

    const hasChanged = model.hasChanged;

    if (hasChanged.coins) {
      this.updateCoinsLists(model);
      hasChanged.coins = false;
    }
    if (hasChanged.currencies) {
      this.updateCryptosList(model);
      this.updateCryptosFiltered(model);
      this.updatePagination(model, 'cryptos');
      this.updateFiatsList(model);
      this.updateFiatsFiltered(model);
      this.updatePagination(model, 'fiats');
      hasChanged.cryptos.filter = false;
      hasChanged.fiats.filter = false;
      hasChanged.cryptos.pagination = false;
      hasChanged.fiats.pagination = false;
      hasChanged.currencies = false;
    }
    if (hasChanged.cryptos.filter) {
      this.updateCryptosFiltered(model);
      this.updatePagination(model, 'cryptos');
      hasChanged.cryptos.pagination = false;
      hasChanged.cryptos.filter = false;
    }
    if (hasChanged.fiats.filter) {
      this.updateFiatsFiltered(model);
      this.updatePagination(model, 'fiats');
      hasChanged.fiats.pagination = false;
      hasChanged.fiats.filter = false;
    }
    if (hasChanged.cryptos.pagination) {
      this.updatePagination(model, 'cryptos');
      hasChanged.cryptos.pagination = false;
    }
    if (hasChanged.fiats.pagination) {
      this.updatePagination(model, 'fiats');
      hasChanged.fiats.pagination = false;
    }
    if (hasChanged.cryptos.sort) {
      this.updateSortOrder(model, 'cryptos')
      hasChanged.cryptos.sort = false;
    }
    if (hasChanged.fiats.sort) {
      this.updateSortOrder(model, 'fiats')
      hasChanged.fiats.sort = false;
    }

    this.samRepresent(model);
    // this.samNap(model);
  },

  // Met à jour l'état de l'application, construit le code HTML correspondant,
  // et demande son affichage.
  samRepresent(model) {

    this.representations.currencies  = view.currenciesUI(model,this);
    this.representations.preferences = view.preferencesUI(model,this);
    this.representations.wallet      = view.walletUI(model,this);
    this.representations.app         = view.appUI(model, this);

    view.samDisplay(model.ui.sectionId, this.representations.app);
  },

  updateCoinsLists(model) {
    coins = model.config.coins;
    this.data.coins.allCodes       = Object.keys(coins).sort();
    this.data.coins.posValueCodes  = Object.entries(coins).filter( v => v[1].quantity >  0 ).map( v => v[0] ).sort();
    this.data.coins.nullValueCodes = Object.entries(coins).filter( v => v[1].quantity == 0 ).map( v => v[0] ).sort();

    if (model.config.debug) {
      console.log('state.samUpdate - coins.allCodes       :', this.data.coins.allCodes);
      console.log('state.samUpdate - coins.posValueCodes  :', this.data.coins.posValueCodes);
      console.log('state.samUpdate - coins.nullValueCodes :', this.data.coins.nullValueCodes);
    }
  },

  updateCryptosList(model) {
    const dataMode = model.config.dataMode;
    const list = model.data[dataMode].list.crypto;
    const live = model.data[dataMode].live.rates;

    const clist = {};
    for (let key in list) {
      const dlist = list[key];
      const dlive = live[key];
      clist[key] = {
        code    : dlist.symbol,
        name    : dlist.name,
        icon_url: dlist.icon_url,
        price   : dlive.rate,
        change  : dlive.change,
      };
    }
    this.data.cryptos.list    = clist;
    this.data.cryptos.listNum = Object.keys(clist).length;

    if (model.config.debug)
    console.log('state.samUpdate - cryptos.list :', this.data.cryptos.listNum, list);
  },

  updateFiatsList(model) {
    const dataMode = model.config.dataMode;
    const list = model.data[dataMode].list.fiat;
    const fiatsInfo = model.data.fiatsInfo;

    const clist = {};
    for (let key in list) {
      const dlist = list[key];
      let dfiat = fiatsInfo[key];
      if (!dfiat) dfiat = { symbol: key };
      clist[key] = {
        code  : key,
        name  : dlist,
        symbol: dfiat.symbol,
      };
    }
    this.data.fiats.list = clist;
    this.data.fiats.listNum = Object.keys(clist).length;

    if (model.config.debug)
      console.log('state.samUpdate - fiats.list :', this.data.fiats.listNum, clist);
  },

updateCryptosFiltered(model) {
  const cryptoFilters = model.ui.currenciesCard.tabs.cryptos.filters;
  const filteredCryptos = Object.values(this.data.cryptos.list).filter(crypto => {
    const matchesCode = crypto.code.toUpperCase().includes(cryptoFilters.text.toUpperCase());
    const matchesName = crypto.name.toUpperCase().includes(cryptoFilters.text.toUpperCase());
    const meetsPriceCriteria = crypto.price >= cryptoFilters.price;
    return (matchesCode || matchesName) && meetsPriceCriteria;
  });
  this.data.cryptos.filtered = filteredCryptos;
  this.data.cryptos.filteredNum = filteredCryptos.length;
  if (model.config.debug) {
    console.log("state.samUpdate - cryptos.filtered:", filteredCryptos);
  }
},

updateFiatsFiltered(model) {
  const fiatFilters = model.ui.currenciesCard.tabs.fiats.filters;
  const filteredFiats = Object.values(this.data.fiats.list).filter(fiat => {
    const matchesCode = fiat.code.toUpperCase().includes(fiatFilters.text.toUpperCase());
    const matchesName = fiat.name.toUpperCase().includes(fiatFilters.text.toUpperCase());
    return matchesCode || matchesName;
  });
  this.data.fiats.filtered = filteredFiats;
  this.data.fiats.filteredNum = filteredFiats.length;
  if (model.config.debug) {
    console.log("state.samUpdate - fiats.filtered:", filteredFiats);
  }
},

updateSortOrder(model, currency) {
  const sortOrderSettings = model.ui.currenciesCard.tabs[currency].sort;
  const sortColumn = sortOrderSettings.columns[sortOrderSettings.column];
  const sortOrder = sortOrderSettings.incOrder[sortOrderSettings.column] ? 1 : -1;
  this.data[currency].filtered.sort((currencyA, currencyB) => {
    switch (typeof currencyA[sortColumn]) {
      case "number":
        return (currencyA[sortColumn] - currencyB[sortColumn]) * sortOrder;
      case "string":
        return currencyA[sortColumn].localeCompare(currencyB[sortColumn]) * sortOrder;
    }
  });
  if (model.config.debug) {
    console.log(`state.samUpdate - ${currency}.filtered:`, this.data[currency].filtered);
  }
},

updatePagination(model, currency) {
  const paginationSettings = model.ui.currenciesCard.tabs[currency].pagination;
  const totalFilteredItems = this.data[currency].filteredNum;
  const rowsPerPage = paginationSettings.rowsPerPage[paginationSettings.rowsPerPageIndex];
  const totalPages = Math.ceil(totalFilteredItems / rowsPerPage);
  this.ui.currenciesCard.tabs[currency].pagination.nbPages = totalPages;
  if (paginationSettings.currentPage > totalPages) {
    paginationSettings.currentPage = totalPages;
  }
  if (model.config.debug) {
    console.log(`state.samUpdate - updatePagination ${currency}`);
  }
}
}
