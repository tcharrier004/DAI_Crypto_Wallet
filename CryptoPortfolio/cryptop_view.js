//--------------------------------------------------------------------- View ---
// Génération de portions en HTML et affichage
//
view = {

  // Injecte le HTML dans une balise de la page Web.
  samDisplay(sectionId, representation) {
    const section = document.getElementById(sectionId);
    section.innerHTML = representation;
  },

  // Astuce : Pour avoir la coloration syntaxique du HTML avec l'extension lit-html dans VSCode
  // https://marketplace.visualstudio.com/items?itemName=bierner.lit-html
  // utiliser this.html`<h1>Hello World</h1>` en remplacement de `<h1>Hello World</h1>`
  html([str, ...strs], ...vals) {
    return strs.reduce((acc, v, i) => acc + vals[i] + v, str);
  },

  // Renvoit le HTML de l'interface complète de l'application
  appUI(model, state) {
    const configUI = this.configsChooserUI();
    return `
    <div class="container">
      ${configUI}
      <h1 class="text-center">Portfolio Cryptos</h1>
      <br />
      <div class="row">
        <div class="col-lg-6">
            ${state.representations.currencies}
        <br />
        </div>

        <div class="col-lg-6">
          ${state.representations.preferences}
          <br />
          ${state.representations.wallet}
          <br />
        </div>
      </div>
    </div>
    `;
  },
  configsChooserUI() {
    const options = Object.keys(configs).map(configKey => {
      const selected = configsSelected == configKey ? "selected=\"selected\"" : '';
      return `
      <option ${selected}>${configKey}</option>
      `;
    }).join("\n");

    return `
    <div class="row">
      <div class="col-md-7"></div>
      <div class="col-md-5">
      <br />
        <div class="d-flex justify-content-end">
          <div class="input-group">
            <div class="input-group-prepend ">
              <label class="input-group-text">Initial Config:</label>
            </div>
            <select class="custom-select" onchange="actions.reinit({e:event})">
              ${options}
            </select>
          </div>
        </div>
      </div>
    </div>
    <br />
    `;
  },

  currenciesUI(model, state) {
    const selectedTab = model.ui.currenciesCard.selectedTab;
    switch (selectedTab) {
      case "cryptos":
        return this.currenciesCryptosUI(model, state);
        break;
      case "fiats":
        return this.currenciesFiatsUI(model, state);
        break;
      default:
        console.error("view.currenciesUI() : unknown tab name: ", selectedTab);
        return "<p>Error in view.currenciesUI()</p>";
    }
  },

    currenciesCryptosUI(model, state) {
      const filteredCryptosNum = state.data.cryptos.filteredNum;
      const listCryptosNum = state.data.cryptos.listNum;
      const filteredFiatsNum = state.data.fiats.filteredNum;
      const listFiatsNum = state.data.fiats.listNum;
      const cryptoFiltersText = model.ui.currenciesCard.tabs.cryptos.filters.text;
      const cryptoFiltersPrice = model.ui.currenciesCard.tabs.cryptos.filters.price;
      const posValueCodes = state.data.coins.posValueCodes;
      const allCodes = state.data.coins.allCodes;
      const changeSigns = ["↘", "∼", "↗"];
      const paginationData = model.ui.currenciesCard.tabs.cryptos.pagination;
      const currentPageIndex = paginationData.currentPage - 1;
      const rowsPerPage = paginationData.rowsPerPage[paginationData.rowsPerPageIndex];
      const startIndex = rowsPerPage * currentPageIndex;
      const endIndex = startIndex + rowsPerPage;
      const tableRows = state.data.cryptos.filtered.slice(startIndex, endIndex).map(crypto => {
        const bgClass = posValueCodes.includes(crypto.code) ? "bg-success text-light" : allCodes.includes(crypto.code) ? "bg-warning" : '';
        const changeSymbol = changeSigns[Math.sign(crypto.change) + 1];
        return `
          <tr class="${bgClass}" onclick="actions.selectCurrency({type:'cryptos',code:'${crypto.code}'})">
            <td class="text-center">
              <span class="badge badge-pill badge-light">
                <img src="${crypto.icon_url}" />
                ${crypto.code} </span></td>
            <td><b>${crypto.name}</b></td>
            <td class="text-right"><b>${crypto.price.toFixed(2)}</b></td>
            <td class="text-right">${crypto.change.toFixed(3)} ${changeSymbol}</td>
          </tr>
          `;
      }).join("\n");
      const favoriteCodes = allCodes.map(code => {
        const badgeClass = posValueCodes.includes(code) ? "badge-success" : "badge-warning";
        return `
          <span class="badge ${badgeClass}">${code}</span>
          `;
      }).join("\n");
      const paginationUI = this.paginationUI(model, state, "cryptos");
      return `
        <div class="card border-secondary" model="currencies">
          <div class="card-header">
            <ul class="nav nav-pills card-header-tabs">
              <li class="nav-item">
                <a class="nav-link active" href="#currencies">
                  Cryptos <span
                    class="badge badge-light">${filteredCryptosNum} / ${listCryptosNum}</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link text-secondary" href="#currencies" onclick="actions.changeTab({tab:'currenciesFiats'})">
                  Monnaies cibles
                  <span class="badge badge-secondary">${filteredFiatsNum} / ${listFiatsNum}</span></a>
              </li>
            </ul>
          </div>
          <div class="card-body">
            <div class="input-group">
              <div class="input-group-append">
                <span class="input-group-text">Filtres : </span>
              </div>
              <input value="${cryptoFiltersText}" model="filterText" type="text" class="form-control"
                placeholder="code ou nom..." onchange="actions.updateFilter({e:event, currency:'cryptos'})" />
              <div class="input-group-append">
                <span class="input-group-text">Prix &gt; </span>
              </div>
              <input model="filterSup" type="number" class="form-control"
                value="${cryptoFiltersPrice}" min="0" onchange="actions.updatePriceFilter({e:event})"/>
            </div> <br />
            <div class="table-responsive">
              <table class="col-12 table table-sm table-bordered">
                <thead>
                  <th class="align-mmodeldle text-center col-2">
                    <a href="#currencies" onclick="actions.sort({currency:'cryptos', column:0})">Code</a>
                  </th>
                  <th class="align-mmodeldle text-center col-5">
                    <a href="#currencies" onclick="actions.sort({currency:'cryptos', column:1})">Nom</a>
                  </th>
                  <th class="align-mmodeldle text-center col-2">
                    <a href="#currencies" onclick="actions.sort({currency:'cryptos', column:2})">Prix</a>
                  </th>
                  <th class="align-mmodeldle text-center col-3">
                    <a href="#currencies" onclick="actions.sort({currency:'cryptos', column:3})">Variation</a>
                  </th>
                </thead>
                ${tableRows}
              </table>
            </div>
            ${paginationUI}
          </div>
          <div class="card-footer text-muted"> Cryptos préférées :
            ${favoriteCodes}
          </div>
        </div>
        `;
    },
    paginationUI(model, state, currency) {
        const paginationConfig = model.ui.currenciesCard.tabs[currency].pagination;
        const totalPages = state.ui.currenciesCard.tabs[currency].pagination.nbPages;
        const rowsPerPageOptions = paginationConfig.rowsPerPage.map((option, index) => {
            const selectedAttribute = paginationConfig.rowsPerPageIndex === index ? "selected=\"selected\"" : '';
            return `
          <option ${selectedAttribute} value="${index}">${option}</option>
          `;
        }).join("\n");
        const currentPage = paginationConfig.currentPage;
        const maxPages = paginationConfig.maxPages;
        let startPage = Math.max(currentPage - Math.floor(maxPages / 2) + 1, 1);
        const endPage = Math.min(startPage + maxPages - 1, totalPages);
        startPage = Math.max(1, endPage - maxPages + 1);
        let pageButtons = '';
        for (let pageIndex = startPage; pageIndex <= endPage; pageIndex++) {
            const activeClass = currentPage === pageIndex ? "active" : '';
            pageButtons += `
          <li class="page-item ${activeClass}">
            <a class="page-link" href="#currencies" onclick="actions.changePage({index:${pageIndex},currency:'${currency}'})">${pageIndex}</a>
          </li>
          `;
        }
        const prevPage = Math.max(1, currentPage - 1);
        const nextPage = Math.min(currentPage + 1, totalPages);
        const isFirstPage = currentPage === 1;
        const isLastPage = currentPage === totalPages;
        const prevDisabledClass = isFirstPage ? "disabled" : '';
        const nextDisabledClass = isLastPage ? "disabled" : '';
        return `
        <section model="pagination">
          <div class="row justify-content-center">
            <nav class="col-auto">
              <ul class="pagination">
                <li class="page-item ${prevDisabledClass}">
                  <a onclick="actions.changePage({index:${prevPage},currency:'${currency}'})"
                    class="page-link" href="#currencies">&lt;</a>
                </li>
                ${pageButtons}
                <li class="page-item ${nextDisabledClass}">
                  <a onclick="actions.changePage({index:${nextPage},currency:'${currency}'})"
                    class="page-link" href="#currencies">&gt;</a>
                </li>
              </ul>
            </nav>
            <div class="col-auto">
              <div class="input-group mb-3">
                <select class="custom-select" model="selectTo"
                  onchange="actions.changeRowsPerPage({e:event, currency:'${currency}'})">
                  ${rowsPerPageOptions}
                </select>
                <div class="input-group-append">
                  <span class="input-group-text">par page</span>
                </div>
              </div>
            </div>
          </div>
        </section>
        `;
    },

    currenciesFiatsUI(model, state) {
        const cryptosFilteredNum = state.data.cryptos.filteredNum;
        const cryptosListNum = state.data.cryptos.listNum;
        const fiatsFilteredNum = state.data.fiats.filteredNum;
        const fiatsListNum = state.data.fiats.listNum;
        const fiatsFilterText = model.ui.currenciesCard.tabs.fiats.filters.text;
        const activeTarget = model.config.targets.active;
        const uniqueTargets = mergeUnique(model.config.targets.list, [activeTarget]).sort();
        const paginationConfig = model.ui.currenciesCard.tabs.fiats.pagination;
        const currentPageIndex = paginationConfig.currentPage - 1;
        const rowsPerPageIndex = paginationConfig.rowsPerPage[paginationConfig.rowsPerPageIndex];
        const startIndex = rowsPerPageIndex * currentPageIndex;
        const endIndex = startIndex + rowsPerPageIndex;
        const filteredFiats = state.data.fiats.filtered.slice(startIndex, endIndex).map(fiat => {
            const backgroundClass = activeTarget == fiat.code ? "bg-success text-light" : uniqueTargets.includes(fiat.code) ? "bg-warning" : '';
            return `
          <tr class="${backgroundClass}" onclick="actions.selectCurrency({type:'fiats',code:'${fiat.code}'})">
            <td class="text-center">${fiat.code}</td>
            <td><b>${fiat.name}</b></td>
            <td class="text-center">${fiat.symbol}</td>
          </tr>
          `;
        }).join("\n");
        const favoriteFiats = uniqueTargets.map(target => {
            const badgeClass = activeTarget.includes(target) ? "badge-success" : "badge-warning";
            return `
          <span class="badge ${badgeClass}">${target}</span>
          `;
        }).join("\n");
        const paginationSection = this.paginationUI(model, state, "fiats");
        return `
        <div class="card border-secondary" model="currencies">
          <div class="card-header">
            <ul class="nav nav-pills card-header-tabs">
              <li class="nav-item">
                <a class="nav-link text-secondary" href="#currencies" onclick="actions.changeTab({tab:'currenciesCryptos'})">
                  Cryptos <span class="badge badge-secondary">${cryptosFilteredNum} / ${cryptosListNum}</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" href="#currencies">Monnaies cibles <span class="badge badge-light">${fiatsFilteredNum} / ${fiatsListNum}</span></a>
              </li>
            </ul>
          </div>
          <div class="card-body">
            <div class="input-group">
              <div class="input-group-append">
                <span class="input-group-text">Filtres : </span>
              </div>
              <input value="${fiatsFilterText}" model="filterText" type="text" class="form-control"
                placeholder="code ou nom..." onchange="actions.updateFilter({e:event, currency:'fiats'})" />
            </div> <br />
            <div class="table-responsive">
              <table class="col-12 table table-sm table-bordered">
                <thead>
                  <th class="align-mmodeldle text-center col-3">
                    <a href="#currencies" onclick="actions.sort({currency:'fiats', column:0})">
                    Code</a>
                  </th>
                  <th class="align-mmodeldle text-center col-6">
                    <a href="#currencies" onclick="actions.sort({currency:'fiats', column:1})">
                    Nom</a>
                  </th>
                  <th class="align-mmodeldle text-center col-3">
                    <a href="#currencies" onclick="actions.sort({currency:'fiats', column:2})">
                    Symbole</a>
                  </th>
                </thead>
                ${filteredFiats}
              </table>
            </div>
            ${paginationSection}
          </div>
          <div class="card-footer text-muted"> Monnaies préférées :
            ${favoriteFiats}
          </div>
        </div>
        `;
    },

    preferencesUI(model, state) {
    },

walletUI(model, state) {
    const selectedTab = model.ui.walletCard.selectedTab;
    switch (selectedTab) {
        case "portfolio":
            return this.walletPortfolioUI(model, state);
            break;
        case "ajouter":
            return this.walletAjouterUI(model, state);
            break;
        default:
            console.error("view.currenciesUI() : unknown tab name: ", selectedTab);
            return "<p>Error in view.currenciesUI()</p>";
    }
},

walletPortfolioUI(model, state) {
    const coinsConfig = model.config.coins;
    const cryptosList = state.data.cryptos.list;
    const activeTarget = model.config.targets.active;
    let hasNewQuantity = false;
    let hasInvalmodelQuantity = false;
    let totalValue = 0;

    const tableRows = state.data.coins.posValueCodes.map(coinCode => {
        const coinConfig = coinsConfig[coinCode];
        const cryptoInfo = cryptosList[coinCode];
        const hasNewQuantityValue = coinConfig.quantityNew !== '';
        const quantityValue = hasNewQuantityValue ? coinConfig.quantityNew : coinConfig.quantity;
        const isInvalmodelQuantity = isNaN(parseFloat(quantityValue)) || parseFloat(quantityValue) < 0;

        if (hasNewQuantityValue) {
            hasNewQuantity = true;
        }

        const textClass = isInvalmodelQuantity ? "text-danger" : hasNewQuantityValue ? "text-primary" : '';
        const inputAttributes = `class="form-control ${textClass}" value="${quantityValue}"`;
        let totalValueText = 0;
        let totalTextClass = '';
        
        if (isInvalmodelQuantity) {
            totalValueText = "???";
            totalTextClass = "text-danger";
            hasInvalmodelQuantity = true;
        } else {
            totalValueText = (parseFloat(quantityValue) * cryptoInfo.price).toFixed(2);
            totalValue += parseFloat(totalValueText);
            totalTextClass = hasNewQuantityValue ? "text-primary" : '';
        }

        return `
      <tr>
        <td class="text-center">
          <span class="badge badge-pill badge-light">
            <img src="${cryptoInfo.icon_url}" />
            ${cryptoInfo.code} </span></td>
        <td><b>${cryptoInfo.name}</b></td>
        <td class="text-right">${cryptoInfo.price.toFixed(2)}</td>
        <td class="text-right">
          <input type="text" ${inputAttributes} onchange="actions.changeCoinQuantity({e:event, code:'${cryptoInfo.code}'})" />
        </td>
        <td class="text-right"><span class="${totalTextClass}"><b>${totalValueText}</b></span></td>
      </tr>
      `;
    }).join("\n");

    const isConfirmEnabled = hasNewQuantity && !hasInvalmodelQuantity;
    const confirmButtonClass = isConfirmEnabled ? "btn-primary" : "disabled";
    const cancelButtonClass = isConfirmEnabled || hasInvalmodelQuantity ? "btn-secondary" : "disabled";
    const totalBadgeClass = hasNewQuantity ? "badge-primary" : "badge-success";
    const totalCoins = state.data.coins.posValueCodes.length;
    const nullCoins = state.data.coins.nullValueCodes.length;

    return `
    <div class="card border-secondary text-center" model="wallet">
      <div class="card-header">
        <ul class="nav nav-pills card-header-tabs">
          <li class="nav-item">
            <a class="nav-link active" href="#wallet">Portfolio <span class="badge badge-light">${totalCoins}</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-secondary" href="#wallet" onclick="actions.changeTab({tab:'walletAjouter'})">
            Ajouter <span class="badge badge-secondary">${nullCoins}</span></a>
          </li>
        </ul>
      </div>
      <div class="card-body text-center">

        <br />
        <div class="table-responsive">
          <table class="col-12 table table-sm table-bordered">
            <thead>
              <th class="align-mmodeldle text-center col-1"> Code </th>
              <th class="align-mmodeldle text-center col-4"> Nom </th>
              <th class="align-mmodeldle text-center col-2"> Prix </th>
              <th class="align-mmodeldle text-center col-3"> Qté </th>
              <th class="align-mmodeldle text-center col-2"> Total </th>
            </thead>
            ${tableRows}
          </table>
        </div>

        <div class="input-group d-flex justify-content-end">
          <div class="input-group-prepend">
            <button onclick="actions.confirmCoinsEdition({which:'posQuantities'})"
              class="btn ${confirmButtonClass}">Confirmer</button>
          </div>
          <div class="input-group-append">
            <button onclick="actions.cancelCoinsEdition({which:'posQuantities'})"
              class="btn ${cancelButtonClass}">Annuler</button>
          </div>
        </div>

      </div>

      <div class="card-footer">
        <h3><span class="badge ${totalBadgeClass}">Total : ${totalValue.toFixed(2)} ${activeTarget}</span></h3>
      </div>
    </div>
    `;
},
walletAjouterUI(model, state) {
  const coinConfigs = model.coins;
  const cryptoList = state.data.cryptos.list;
  const activeTarget = model.config.targets.active;
  let hasNewQuantity = false;
  let hasError = false;
  let totalValue = 0;
  const coinRowsHTML = state.data.coins.nullValueCodes.map(coinCode => {
    const coinConfig = model.config.coins[coinCode];
    const cryptoEntry = cryptoList[coinCode];
    const isQuantityNew = coinConfig.quantityNew !== '';
    const coinQuantity = isQuantityNew ? coinConfig.quantityNew : coinConfig.quantity;
    const isInvalmodelQuantity = isNaN(parseFloat(coinQuantity)) || parseFloat(coinQuantity) < 0;
    if (isQuantityNew) {
      hasNewQuantity = true;
    }
    const quantityClass = isInvalmodelQuantity ? "text-danger" : isQuantityNew ? "text-primary" : '';
    const inputAttributes = `class="form-control ${quantityClass}" value="${coinQuantity}"`;
    let totalCoinValue = 0;
    let totalValueClass = '';
    if (isInvalmodelQuantity) {
      totalCoinValue = "???";
      totalValueClass = "text-danger";
      hasError = true;
    } else {
      totalCoinValue = (parseFloat(coinQuantity) * cryptoEntry.price).toFixed(2);
      totalValue += parseFloat(totalCoinValue);
      totalValueClass = isQuantityNew ? "text-primary" : '';
    }
    return `
      <tr>
        <td class="text-center">
          <span class="badge badge-pill badge-light">
            <img src="${cryptoEntry.icon_url}" />
            ${cryptoEntry.code} </span></td>
        <td><b>${cryptoEntry.name}</b></td>
        <td class="text-right">${cryptoEntry.price.toFixed(2)}</td>
        <td class="text-right">
          <input type="text" ${inputAttributes} onchange="actions.changeCoinQuantity({e:event, code:'${cryptoEntry.code}'})" />
        </td>
        <td class="text-right"><span class="${totalValueClass}"><b>${totalCoinValue}</b></span></td>
      </tr>
    `;
  }).join("\n");
  const hasValmodelChanges = hasNewQuantity && !hasError;
  const confirmButtonClass = hasValmodelChanges ? "btn-primary" : "disabled";
  const cancelButtonClass = hasValmodelChanges || hasError ? "btn-secondary" : "disabled";
  const totalValueBadgeClass = hasNewQuantity ? "badge-primary" : "badge-success";
  const numberOfCoins = state.data.coins.nullValueCodes.length;
  return `
    <div class="card border-secondary text-center" model="wallet">
      <div class="card-header">
        <ul class="nav nav-pills card-header-tabs">
          <li class="nav-item">
            <a class="nav-link text-secondary" href="#wallet" onclick="actions.changeTab({tab:'walletPortfolio'})">
              Portfolio <span class="badge badge-secondary">${numberOfCoins}</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="#wallet">Add <span
                class="badge badge-light">${numberOfCoins}</span></a>
          </li>
        </ul>
      </div>
      <div class="card-body">
        <br />
        <div class="table-responsive">
          <table class="col-12 table table-sm table-bordered">
            <thead>
              <th class="align-mmodeldle text-center col-1"> Code </th>
              <th class="align-mmodeldle text-center col-4"> Name </th>
              <th class="align-mmodeldle text-center col-2"> Price </th>
              <th class="align-mmodeldle text-center col-3"> Quantity </th>
              <th class="align-mmodeldle text-center col-2"> Total </th>
            </thead>
            ${coinRowsHTML}
          </table>
        </div>
        <div class="input-group d-flex justify-content-end">
          <div class="input-group-prepend">
            <button onclick="actions.confirmCoinsEdition({which:'zeroQuantities'})"
             class="btn ${confirmButtonClass}">Confirm</button>
          </div>
          <div class="input-group-append">
            <button onclick="actions.cancelCoinsEdition({which:'zeroQuantities'})"
              class="btn ${cancelButtonClass}">Cancel</button>
          </div>
        </div>
      </div>
      <div class="card-footer">
        <h3><span class="badge ${totalValueBadgeClass}">Total : ${totalValue.toFixed(2)} ${activeTarget}</span></h3>
      </div>
    </div>
  `;
},

};
