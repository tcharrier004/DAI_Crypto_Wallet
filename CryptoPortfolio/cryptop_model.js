//-------------------------------------------------------------------- Model ---
// Unique source de vérité de l'application
//
model = {

  config: {},
  data : {},
  ui   : {},

  // Demande au modèle de se mettre à jour en fonction des données qu'on
  // lui présente.
  // l'argument data est un objet confectionné dans les actions.
  // Les propriétés de data apportent les modifications à faire sur le modèle.
  samPresent(data) {

    switch (data.do) {

      case 'init': {
        Object.assign(this, data.config);
        const conf = this.config;
        conf.targets.list = mergeUnique([conf.targets.wished], conf.targets.list).sort();
        const isOnline = conf.dataMode == 'online';
        conf.targets.active = isOnline ? conf.targets.wished : this.data.offline.live.target;
        this.hasChanged.currencies = true;
        if (conf.debug) console.log('model.samPresent - init - targets.list  : ', conf.targets.list);
        if (conf.debug) console.log('model.samPresent - init - targets.active: ', conf.targets.active);
      } break;

      case 'updateCurrenciesData': {
        this.data.online = data.currenciesData;
        this.config.targets.active = data.currenciesData.live.target;
        this.hasChanged.currencies = true;
      } break;

      case 'changeDataMode': {
        this.config.dataMode = data.dataMode;
        if (data.dataMode == 'offline') {
          this.config.targets.active = this.data.offline.live.target;
          this.hasChanged.currencies = true;
        }
      } break;

      case 'changeTab': {
        switch (data.tab) {
          case 'currenciesCryptos':
            this.ui.currenciesCard.selectedTab = 'cryptos';
            break;
          case 'currenciesFiats':
            this.ui.currenciesCard.selectedTab = 'fiats';
            break;
          case 'walletPortfolio':
            this.ui.walletCard.selectedTab = 'portfolio';
            break;
          case 'walletAjouter':
            this.ui.walletCard.selectedTab = 'ajouter';
            break;
            default:
        }
      } break;

     case "updateFilter":
        {
          const MYCURRENCY = this.ui.currenciesCard.tabs[data.currency];
          MYCURRENCY.filters.text = data.filterText;
          MYCURRENCY.pagination.currentPage = 1;
          this.hasChanged[data.currency].filter = true;
        }
        break;
      case "updatePriceFilter":
        {
          const CRYPTOS = this.ui.currenciesCard.tabs.cryptos;
          CRYPTOS.filters.price = data.filterPrice;
          CRYPTOS.pagination.currentPage = 1;
          this.hasChanged.cryptos.filter = true;
        }
        break;
      case "sort":
        {
          console.log("sort : ", data);
          const CURRENCY = this.ui.currenciesCard.tabs[data.currency];
          const CURRSORTED = CURRENCY.sort;
          if (CURRSORTED.column == data.column) {
            CURRSORTED.incOrder[data.column] = !CURRSORTED.incOrder[data.column];
          }
          CURRSORTED.column = data.column;
          CURRENCY.pagination.currentPage = 1;
          console.log("sort : ", CURRSORTED);
          this.hasChanged[data.currency].sort = true;
          this.hasChanged[data.currency].pagination = true;
        }
        break;
      case "changePage":
        {
          const PAGINATION = this.ui.currenciesCard.tabs[data.currency].pagination;
          PAGINATION.currentPage = data.index;
        }
        break;
      case "changeRowsPerPage":
        {
          const PAGINATION = this.ui.currenciesCard.tabs[data.currency].pagination;
          PAGINATION.rowsPerPageIndex = data.rowsPerPageIndex;
          this.hasChanged[data.currency].pagination = true;
        }
        break;
      case "selectCurrency":
        {
          const CODE = data.code;
          switch (data.type) {
            case "cryptos":
              {
                const COINS = this.config.coins;
                let COINS_CODE = COINS[CODE];
                if (!COINS_CODE) {
                  COINS[CODE] = {
                    "quantity": 0,
                    "quantityNew": ''
                  };
                  this.hasChanged.coins = true;
                } else if (COINS_CODE.quantity == 0 && COINS_CODE.quantityNew == '') {
                  delete COINS[CODE];
                  this.hasChanged.coins = true;
                }
              }
              break;
            case "fiats":
              {
                const TARGETS_LIST = this.config.targets.list;
                const TARGETS_ACTIVE = this.config.targets.active;
                if (TARGETS_LIST.includes(CODE)) {
                  if (CODE != TARGETS_ACTIVE) {
                    this.config.targets.list = TARGETS_LIST.filter(v => v != CODE);
                  }
                } else {
                  TARGETS_LIST.push(CODE);
                  TARGETS_LIST.sort();
                }
              }
              break;
            default:
          }
        }
        break;
      case "changeCoinQuantity":
        {
          const COINS = this.config.coins;
          const COINS_CODE = COINS[data.code];
          COINS_CODE.quantityNew = data.quantityNew;
          this.hasChanged.coins = true;
        }
        break;
      case "cancelCoinsEdition":
        {
          const COINS = this.config.coins;
          Object.values(COINS).forEach(v => {
            const QUANTITY = data.which == "zeroQuantities" && v.quantity == 0 || data.which == "posQuantities" && v.quantity > 0;
            if (QUANTITY) {
              v.quantityNew = '';
            }
          });
        }
        break;
      case "confirmCoinsEdition":
        {
          const COINS = this.config.coins;
          Object.values(COINS).forEach(v => {
            const QUANTITY = data.which == "zeroQuantities" && v.quantity == 0 || data.which == "posQuantities" && v.quantity > 0;
            if (QUANTITY && parseFloat(v.quantityNew) >= 0) {
              v.quantity = parseFloat(v.quantityNew);
              v.quantityNew = '';
              this.hasChanged.coins = true;
            }
          });
        }
        break;
      default:
        console.error("model.samPresent(), unknown do: '" + data["do"] + "' ");
    }
    state.samUpdate(this);
  }
};
